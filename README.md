## Test Nodemon Project

This is a basic project for testing javascript within a node environment.

To make a local copy of project, open a folder/directory of your choice and type:
**it clone https://tomagnew@bitbucket.org/tomagnew/test-nodemon.git**

Then enter directory (**cd test-nodemon**) and type npm install. This will install nodemon package.

To test that everything is running, type **npm start**. This should launch app.js with nodemon watching for changes to it or any other file within the working directory.